import React, { useState } from 'react'
import ReactDOM from 'react-dom/client'
import logo from './assets/logo.svg'
import Welcome from './pages/Welcome'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://react.dev" target="_blank">
          <img src={logo} className="logo react" alt="Logo" />
        </a>
      </div>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          Click to support: {count}
        </button>
      </div>
      <p className="read-the-docs">
        Creating the future of digital experiences
      </p>
    </>
  )
}

export default App

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Welcome />
  </React.StrictMode>,
)
