﻿import React, { useEffect, useRef, useState } from "react";
import { motion, useTime, useTransform } from "framer-motion";
import logo from '../assets/logo.svg'

export default function Welcome() {

  const mensaje = {
    to: `info@infinitelabs.com.ar`,
    cc: `jwildemer@infinitelabs.com.ar`,
    subject: "Meeting",
    body: `¿Que tal? Vi el anuncio y me gustaría saber más sobre ustedes. Tengo interés en conocer sus propuestas y organizar una reunión esta semana para llevar a cabo un proyecto juntos.\n\nSaludos!`,
  }

  /* Crear un objeto que represente un envío de mail para adjuntar en un link */
  const mailTo = `mailto:${mensaje.to}?cc=${mensaje.cc}&subject=${encodeURIComponent(mensaje.subject)}&body=${encodeURIComponent(mensaje.body)}`

  return (
    <div className="welcome" style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
      {/* <motion.div
        animate={{
          scale: [1, 1.5, 1],
          rotate: [0, 360, 360],
        }}
        className="logoCss"
        transition={{
          duration: 2,
          ease: "easeInOut",
          times: [0, 0.5, 1],
          repeat: Infinity,
          repeatDelay: 1
        }} /> */}

      <a href={mailTo} target="_blank">
        <img src={logo} className="logo" alt="Logo" />
      </a>

      <h2>Creating the future<br></br>of digital experiences</h2>
      <h4 className="read-the-docs">More info: <a href={mailTo} target="_blank">info@infinitelabs.com.ar</a></h4>
    </div>
  );
}
